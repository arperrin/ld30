///scr_end_game(message) :: Script that ends the game!

end_game = instance_create(1, 1, obj_end_game);
with (end_game)
{
  message = argument0;
}
