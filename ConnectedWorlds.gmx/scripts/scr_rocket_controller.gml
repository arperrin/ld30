///scr_rocket_controller :: Script that handles deployment of rockets

// pick up resources
if (queued_resource != noone && mouse_check_button_pressed(mb_left))
{
  scr_post_message("click");
  queued_ability = instance_place(mouse_x, mouse_y, obj_ability_window);
  if (queued_ability != noone)
  {
    with (queued_ability)
    {
      other.queued_resource_name = name;     
      other.queued_resource_amount = floor(random_range(5, 10));
    }
    queued_resource = instance_create(mouse_x, mouse_y, obj_resource);
  }
}

// create a rocket and free resources
if (queued_resource != noone && mouse_check_button_released(mb_left))
{
  planet = instance_place(mouse_x, mouse_y, obj_planet);
  if (planet != noone)
  {
    // queue a rocket toward that planet
    player = instance_find(obj_omnicorp, 0);
    rocket = instance_create(player.x, player.y, obj_rocket);
    with (rocket)
    {
      name = other.queued_resource_name;
      amount = other.queued_resource_amount;
      target = other.planet;      
    }
    with (queued_resource) 
    {
      instance_destroy();
    }
    with (queued_ability)
    {
      amount -= other.queued_resource_amount;
    }
    queued_resource = noone;
    queued_amount = noone;
  }
}
