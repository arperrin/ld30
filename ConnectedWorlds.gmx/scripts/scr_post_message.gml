///scr_post_message(message) :: Post a message to the status window
status_window = instance_find(obj_message_window, 0);
if (status_window == noone) return 0;

with(status_window)
{
  ds_list_add(message_queue, string(argument0));
}